# Kattis Pizza Delivery

My solution for [Kattis Pizza Delivery](https://kth.kattis.com/problems/pizza), but I get Time Limit Exceeded.<br/>
How can I optimize the code? Any help is appreciated! Merge requests are welcomed!
