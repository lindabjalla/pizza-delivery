package se.mogumogu.pizzadelivery;

import java.util.InputMismatchException;

final class DeliveryCostsCalculator {

    private int areaSizeX;
    private int areaSizeY;
    private int numberOfBlocks;

    private int getNumberOfTestCases(final Kattio kattio) {

        int numberOfTestCases = kattio.getInt();

        if (numberOfTestCases < 1 || numberOfTestCases > 20) {
            throw new InputMismatchException("Number of test cases must be in a range of 1 and 20");
        }

        return numberOfTestCases;
    }

    private void getAreaSize(final Kattio kattio) {

        final int x = kattio.getInt();
        final int y = kattio.getInt();

        if (x < 1 || x > 100 || y < 1 || y > 100) {
            throw new InputMismatchException("Area size of x and y must be in a range of 1 and 100");
        }

        areaSizeX = x;
        areaSizeY = y;
    }

    private int[] getLocation(final Kattio kattio) {

        getAreaSize(kattio);
        numberOfBlocks = areaSizeX * areaSizeY;
        final int[] blocks = new int[numberOfBlocks];

        for (int i = 0; i < numberOfBlocks; i++) {
            final int numberOfDeliveries = kattio.getInt();

            if (numberOfDeliveries < 0 || numberOfDeliveries > 1000) {
                throw new InputMismatchException("Number of deliveries must be in a range of 0 and 1000");
            }

            blocks[i] = numberOfDeliveries;
        }

        return blocks;
    }

    private void outputTotalDeliveryCost(final int[] blocks) {

        int minTotalDeliveryCost = Integer.MAX_VALUE;

        for (int i = 0; i < numberOfBlocks; i++) {

            final int totalDeliveryCost = getTotalDeliveryCost(blocks, minTotalDeliveryCost, findPositionX(i), findPositionY(i));

            if (minTotalDeliveryCost < 1 || (minTotalDeliveryCost > 0 && minTotalDeliveryCost > totalDeliveryCost)) {
                minTotalDeliveryCost = totalDeliveryCost;
            }
        }

        System.out.println(minTotalDeliveryCost + " blocks");
    }

    private int getTotalDeliveryCost(
            final int[] blocks, final int minTotalDeliveryCost, final int kitchenPositionX, final int kitchenPositionY) {

        int totalDeliveryCost = 0;

        for (int i = 0; i < numberOfBlocks; i++) {

            int numberOfDeliveries = blocks[i];

            if (numberOfDeliveries > 0 && minTotalDeliveryCost > totalDeliveryCost) {

                int distance = Math.abs(findPositionX(i) - kitchenPositionX) + Math.abs(findPositionY(i) - kitchenPositionY);
                int deliveryCost = distance * numberOfDeliveries;
                totalDeliveryCost = totalDeliveryCost + deliveryCost;

            } else if (minTotalDeliveryCost <= totalDeliveryCost) {
                return minTotalDeliveryCost;
            }
        }

        return totalDeliveryCost;
    }

    private int findPositionY(final int index) {
        return index / areaSizeX;
    }

    private int findPositionX(final int index) {
        return index % areaSizeX;
    }

    void calculate() {

        final Kattio kattio = new Kattio(System.in);
        int numberOfTestCases = getNumberOfTestCases(kattio);

        while (numberOfTestCases != 0) {
            final int[] blocks = getLocation(kattio);
            outputTotalDeliveryCost(blocks);
            numberOfTestCases--;
        }
    }

    public static void main(String[] args) {

        try {
            final DeliveryCostsCalculator deliveryCostsCalculator = new DeliveryCostsCalculator();
            deliveryCostsCalculator.calculate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
